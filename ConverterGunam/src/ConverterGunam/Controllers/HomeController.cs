﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterGunam.Models;

namespace ConverterGunam.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Gunam";
            ViewData["Result"] = "";

            Conv converter = new Conv();
            return View(converter);
        }

        public IActionResult Convert(Conv converter)
        {
            if (ModelState.IsValid)
            {
                ViewData["Title"] = "Converted by Gunam";
                ViewData["Result"] = "Temperature in C = " +
                    (int)((converter.Temperature_F - 32) * 5.0 / 9.0);
            }
            return View("Index", converter);
        }
    }
}
